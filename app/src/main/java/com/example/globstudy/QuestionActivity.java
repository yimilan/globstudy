package com.example.globstudy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.Objects;

public class QuestionActivity extends AppCompatActivity {

    LinearLayout boxCards;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private static final String TAG = "Question";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        setup();
    }

    private void setup(){
        boxCards = findViewById(R.id.boxCards);
        db.collection("question")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {

                        for (QueryDocumentSnapshot document : task.getResult()) {

                            LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                            LinearLayout  linearLayout = new LinearLayout(QuestionActivity.this);
                            linearLayout.setOrientation(LinearLayout.VERTICAL);
                            linearLayout.setWeightSum(6f);
                            linearLayout.setLayoutParams(LLParams);

                            TextView textViewTitle = new TextView(QuestionActivity.this);
                            textViewTitle.setTextColor(Color.parseColor("#000000"));
                            textViewTitle.setTextAppearance(QuestionActivity.this, R.style.TextAppearance_AppCompat_Headline);
                            textViewTitle.setText(Objects.requireNonNull(document.getData().get("title")).toString());

                            TextView textViewCategory = new TextView(QuestionActivity.this);
                            textViewCategory.setTextColor(Color.parseColor("#000000"));
                            textViewCategory.setTextAppearance(QuestionActivity.this, R.style.TextAppearance_AppCompat_Body2);
                            textViewCategory.setText(Objects.requireNonNull(document.getData().get("content")).toString());

                            TextView textViewContent = new TextView(QuestionActivity.this);
                            textViewContent.setTextColor(Color.parseColor("#000000"));
                            textViewContent.setTextAppearance(QuestionActivity.this, R.style.TextAppearance_AppCompat_Body2);
                            textViewContent.setText(Objects.requireNonNull(document.getData().get("category")).toString());

                            linearLayout.addView(textViewTitle);
                            linearLayout.addView(textViewCategory);
                            linearLayout.addView(textViewContent);

                            boxCards.childDrawableStateChanged(linearLayout);
                            boxCards.addView(linearLayout);

                            Log.d(TAG, document.getId() + " => " + document.getData());
                            Toast.makeText(QuestionActivity.this, Objects.requireNonNull(document.getData().get("title")).toString(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        switch (item.getItemId())
        {
            case R.id.itemNewQuestion:
                Intent intent = new Intent( QuestionActivity.this, NewQuestionActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}