package com.example.globstudy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class NewQuestionActivity extends AppCompatActivity {

    Button new_question;
    EditText editTextTitle;
    EditText editTextContent;
    EditText editTextCategory;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private static final String TAG = "Question";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_question);
        setup();
    }

    private void setup(){
        new_question = findViewById(R.id.newQuestion);
        editTextTitle = findViewById(R.id.editTextTitle);
        editTextContent = findViewById(R.id.editTextContent);
        editTextCategory = findViewById(R.id.editTextCategory);
        Map<String, Object> questionDB = new HashMap<>();

        new_question.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(editTextTitle.getText().toString().trim().length() != 0 && editTextContent.getText().toString().trim().length() != 0
                        && editTextCategory.getText().toString().trim().length() != 0){
                            questionDB.put("title", editTextTitle.getText().toString());
                            questionDB.put("content", editTextContent.getText().toString());
                            questionDB.put("category", editTextCategory.getText().toString());
                            registerQuestion(questionDB);
                            Intent intent = new Intent( NewQuestionActivity.this, QuestionActivity.class);
                            startActivity(intent);
                }
                else{
                    Toast.makeText(NewQuestionActivity.this, "Error to register question.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void registerQuestion(Map qst){
        db.collection("question")
                .add(qst)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        switch (item.getItemId())
        {
            case R.id.itemNewQuestion:
                Intent intent = new Intent( NewQuestionActivity.this, NewQuestionActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}